# mag-boots

This was a halloween project where we mounted powerbanks, wemos D1 controllers and some LED-strips on our boots to make them look like magnetic boots from The Expanse.

## D1?

The microcontrollers communicate over wifi in a low-effort attempt at syncing. The UDP-broadcast address is not really good tho. It is hard coded. If you are planning on using this for anything be prepared to fix some issues and clean up the code a bit. If you'd like to contribute your changes any PRs are welcome. Or just fork it :)

![boots](img/boots.jpg)


