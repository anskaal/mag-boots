#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <FastLED.h>

#ifndef STASSID
#define STASSID "<your-ssid>"
#define STAPSK  "<your-password>"
#endif

#define LED_PIN     8
#define NUM_LEDS    7
CRGB leds[NUM_LEDS];

const byte interruptPin = 4;

const CRGB red = CRGB(255, 0, 0);
const CRGB blue = CRGB(125, 0, 0);
const CRGB off = CRGB(0, 0, 0);

char activeBuffer[] = "on";
char inactiveBuffer[] = "off";

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged\r\n";       // a string to send back

WiFiUDP Udp;
IPAddress IP_Remote(192, 168, 1, 255);

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(STASSID, STAPSK);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(500);
  }
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);

  // LED
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  
  pinMode(interruptPin, INPUT_PULLUP);
  Serial.println("Setup complete!");
}

volatile byte prevState = LOW;
volatile boolean otherBootsOnGround = 0;

bool readUdp() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    
    if (Udp.remoteIP().toString().c_str() == WiFi.localIP().toString().c_str()) {
      return otherBootsOnGround;
    }
    
    // read the packet into packetBufffer
    int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    packetBuffer[n] = 0;
    if (packetSize == 2) {
      otherBootsOnGround = true;
    } else if (packetSize == 3) {
      otherBootsOnGround = false;
    }
  }
  return otherBootsOnGround;
}

void sendUdp(char buf[]) {
  for (int i=0; i < 8; i++) {
    Udp.beginPacket(IP_Remote, 8888);
    Udp.write(buf);
    Udp.endPacket();  
    delay(10);
  }
}

bool readGpio() {
  int state = digitalRead(interruptPin);
  if (prevState != state) {
    if (state == LOW) {
      sendUdp(activeBuffer);
      Serial.println("UDP -> on");
    } else {
      sendUdp(inactiveBuffer);
      Serial.println("UDP -> off");
    }
    prevState = state;
  }
  return !state;
}

void updateLeds(bool isOn, CRGB color) {
  for (int i=0; i < 8; i++) {
      if (isOn) {
          leds[i] = color;
      } else {
          leds[i] = off;
      }
  }
  FastLED.show();
}

void loop() {
  bool isSelfOn = readGpio();
  if (isSelfOn) {
    updateLeds(isSelfOn, red);
  } else {
    int isOtherOn = readUdp();
    updateLeds(isOtherOn, blue);
  }
  delay(100);
}
